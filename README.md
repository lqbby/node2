## 1. 整理以下数组方法，写出数组方法的解释，并使用

### 1. reduce

```js
      // 参数：用于执行每个数组元素的函数。
      // 作用：接收一个函数作为累加器，数组中的每个值（从左到右）开始缩减，最终计算为一个值。
      let arr = [1, 2, 3, 4, 5, 6]
      // 前面两个是必须, 0表示传递给函数的初始值
      arr.reduce((total, currentValue, currentIndex, arr) => {
        // 这里total指初始值, 或者计算结束后的返回值,
        console.log(total) // 0
        console.log(currentValue) // 1
        console.log(currentIndex) // 0
        console.log(arr) // [1,2,3,4,5,6]
      }, 0)
```

### 2. filter

```js
      // 参数: 函数，数组中的每个元素都会执行这个函数
      // 作用: filter() 方法创建一个新的数组，新数组中的元素是通过检查指定数组中符合条件的所有元素。
      // 返回值：返回数组，包含了符合条件的所有元素。如果没有符合条件的元素则返回空数组。
      let ages1 = [32, 33, 16, 40]
      function checkAdult(age) {
        return age >= 18
      }
     

      function myFunction() {
        document.getElementById('demo').innerHTML = ages1.filter(checkAdult)
      } //32,33,40
```
### 3. some

```js
      // 参数: 函数，数组中的每个元素都会执行这个函数
      // 作用: 方法用于检测数组中的元素是否满足指定条件
      // 返回值：布尔值。如果数组中有元素满足条件返回 true，否则返回 false。
      let ages = [3, 10, 18, 20]

      function checkAdult(age) {
        return age >= 18
      }

      function myFunction() {
        document.getElementById('demo').innerHTML = ages.some(checkAdult)
      }
```
### 4. every
```js
      // 参数: 函数，数组中的每个元素都会执行这个函数
      // 作用: 用于检测数组所有元素是否都符合指定条件
      // 返回值：布尔值。如果所有元素都通过检测返回 true，否则返回 false。
      let ages2 = [32, 33, 16, 40]

      function checkAdult(age) {
        return age >= 18
      }

      function myFunction() {
        document.getElementById('demo').innerHTML = ages2.every(checkAdult)
      }
```
### 5. map
```js
      // 参数: 函数，数组中的每个元素都会执行这个函数
      // 作用: 返回一个数组，数组中元素为原始数组的平方根:
      // 返回值：返回一个新数组，数组中的元素为原始数组元素调用函数处理后的值。
      let numbers = [4, 9, 16, 25]

      function myFunction() {
        x = document.getElementById('demo')
        x.innerHTML = numbers.map(Math.sqrt)
      } //2,3,4,5
```
## 2. 整理出来以下会改变原数组的数组方法


### 1.push
```js
      // 参数:  要添加到数组的元素。
      // 作用:  数组中添加新元素
      // 返回值：数组新长度
      let fruits = ['Banana', 'Orange', 'Apple', 'Mango']
      fruits.push('Kiwi')
      console.log(fruits)
```
### 2.pop
```js      
      // 作用: 移除最后一个数组元素
      // 返回值：返回删除的元素。
      let fruits1 = ['Banana', 'Orange', 'Apple', 'Mango']
      fruits1.pop()
```
### 3.unshift
```js
      // 参数: 向数组起始位置添加一个或者多个元素。
      // 作用: 将新项添加到数组起始位置
      // 返回值: 数组新长度
      let fruits2 = ['Banana', 'Orange', 'Apple', 'Mango']
      fruits2.unshift('Lemon', 'Pineapple')
```
### 4. shift
```js
      // 作用: 从数组中移除元素
      // 返回值：数组原来的第一个元素的值（移除的元素）
      let fruits3 = ["Banana", "Orange", "Apple", "Mango"];
      fruits3.shift()
```
### 5. sort
```js      
      // 参数: 规定排序顺序。必须是函数。
      // 作用: 数组排序,排序顺序可以是字母或数字，并按升序或降序。
      // 返回值：对数组的引用。请注意，数组在原数组上进行排序，不生成副本。
      let fruits4 = ["Banana", "Orange", "Apple", "Mango"];
      fruits4.sort();
```
### 6. reverse
```js
      // 作用: 颠倒数组中元素的顺序
      // 返回值：颠倒顺序后的数组
      let fruits5 = ["Banana", "Orange", "Apple", "Mango"];
      fruits5.reverse();
```
### 7. splice
```js
      // 作用: 数组中添加新元素
      // 返回值：如果删除一个元素，则返回一个元素的数组。 如果未删除任何元素，则返回空数组
      let fruits6 = ["Banana", "Orange", "Apple", "Mango"];
      fruits6.splice(2,0,"Lemon","Kiwi");
```